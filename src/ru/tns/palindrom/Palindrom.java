package ru.tns.palindrom;

/*
 * Класс для реализации действий с палиндромом
 *
 * определяет является ли введенная строка палиндромом или нет.
 *
 * @Autor Tropanova N.S.
 */

import java.util.Scanner;

public class Palindrom {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Введите слово(фразу): ");
        String s = scanner.nextLine();
        s = s.replaceAll("[\\d+\\s()?:!.,;{}\"-]", ""); //удаляем все ненужное 
        if (s.toLowerCase().equals((new StringBuffer(s)).reverse().toString().toLowerCase())) { //присваиваем перевернутую строку
            System.out.println("Палиндром! :)");
        } else {
            System.out.println("Не палиндром! :(");
        }
    }
}